import { LIST_TODOS_PENDING, LIST_TODOS_SUCCESS, LIST_TODOS_FAILED, UPDATE_COMPLETED_TODOS } from './Conatants';

import TodoList from './TodosList.json';

export const listTodos = () => (dispatch) => {
    dispatch({ type: LIST_TODOS_PENDING, payload: {} });

    setTimeout(() => {
        dispatch({ type: LIST_TODOS_SUCCESS, payload: { todoList: TodoList } });
    }, 1000);
}

export const updateCompletedList = (data) => (dispatch) => {
    dispatch({ type: UPDATE_COMPLETED_TODOS, payload: { todoList: data } });
}