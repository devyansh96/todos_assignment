import { LIST_TODOS_PENDING, LIST_TODOS_SUCCESS, LIST_TODOS_FAILED, UPDATE_COMPLETED_TODOS } from './Conatants';

const initialState = {
    todos: [],
    isPending: true
}

export const requestTodos = (state = initialState, { type, payload }) => {
    switch (type) {

        case LIST_TODOS_PENDING:
            return { ...state, isPending: true }

        case LIST_TODOS_SUCCESS:
            return { ...state, isPending: false, todos: payload }

        case UPDATE_COMPLETED_TODOS:
            return { ...state, todos: payload }

        default:
            return state
    }
}
