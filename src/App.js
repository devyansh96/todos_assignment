import React from 'react';
import { connect } from 'react-redux';
import Header from './Pages/Header'
import ToDo from './Pages/ToDO';
import './App.css';

import { listTodos, updateCompletedList } from './redux/Actions';

const mapStateToProps = (state) => {
    return {
        todos: state.requestTodos.todos,
        isPending: state.requestTodos.isPending,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRequestTodos: () => dispatch(listTodos()),
        handleSelectTodo: (data) => dispatch(updateCompletedList(data))
    }
}

class App extends React.Component {

    constructor(props) {
        super(props);
        this.handleSelection = this.handleSelection.bind(this);
    }

    componentDidMount() {
        this.props.onRequestTodos();
    }

    handleSelection(todoCategory, datalist, type) {
        var todoIndex = this.props.todos.todoList.findIndex(elm => elm.id == todoCategory.id);

        var tempTodo = [...this.props.todos.todoList];

        var data = [...tempTodo[todoIndex].todos];

        var elemntsIndex = data.findIndex(elm => elm.id == datalist.id);

        if (type == 'add') data[elemntsIndex] = { ...data[elemntsIndex], isCompleted: !data[elemntsIndex].isCompleted };
        else data[elemntsIndex] = { ...data[elemntsIndex], isCompleted: false };

        tempTodo[todoIndex].todos = data;

        var checkActive = tempTodo[todoIndex].todos.findIndex(elm => elm.isCompleted == true);

        if (checkActive == -1) tempTodo[todoIndex].isActive = false;
        else tempTodo[todoIndex].isActive = true;

        this.props.handleSelectTodo(tempTodo);
    }

    render() {
        return (
            <div className="App">
                <Header />
                <ToDo
                    List={this.props.todos}
                    isPending={this.props.isPending}
                    handleSelection={this.handleSelection}
                />
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
