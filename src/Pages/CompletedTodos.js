import React from 'react'

export default function CompletedTodos(props) {
  const { List, handleSelection } = props;

  return (
    <div className="col-md-6">
      <div className="col-md-12 shadow">
        {/* <h4></h4>
                        <div className="noValue">
                            No Value Selected
                            </div> */}
        {List.todoList.filter(elm => elm.isActive == true).map((data) => (
          <div key={data.id}>
            <h4>{data.listname}</h4>
            <div className="form-group">
              {data.todos.filter(elm => elm.isCompleted == true).map((datalist) => (
                <div key={datalist.id} className="form-check">
                  <label className="form-check-label set-label" htmlFor="gridCheck">
                    {datalist.name}<button type="button" className="btn close"
                      data-dismiss="alert" aria-label="Close" onClick={() => handleSelection(data, datalist, 'delete')}>
                      <span aria-hidden="true">×</span>
                    </button>
                  </label>
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}
