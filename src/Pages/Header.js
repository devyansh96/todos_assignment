import React from 'react';
import './PageStyle.css';

export default function Header() {
    return (
        <div id="header">
            <div className="container">
                <div className="row">
                    <div className="col-md-5">
                        <h1 id="logo">Logo</h1>
                    </div>
                    <div className="col-md-7">
                        <ul id="menu" className="float-md-right">
                            <li><a>Home</a></li>
                            <li><a>My PortFolio</a></li>
                            <li><a>Clients</a></li>
                            <li><button className="btn btn-basic">Get in Touch</button></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
