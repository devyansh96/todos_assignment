import React from "react";
import CompletedTodos from "./CompletedTodos";
import './PageStyle.css';
import SelectionList from "./SelectionList";
import Loading from '../shared/assets/loading.gif';


function ToDo(props) {
    const { isPending, List, handleSelection } = props;

    if (isPending == true) {
        return <div>
            <img src={Loading} alt="Data_Loading" />
        </div>
    }

    return (
        <div className="container">
            <div className="row">
                <SelectionList List={List} handleSelection={handleSelection} />

                <CompletedTodos List={List} handleSelection={handleSelection} />
            </div>
        </div>
    );
}


export default ToDo;