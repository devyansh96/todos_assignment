import React from 'react'

export default function SelectionList(props) {
  const { List, handleSelection } = props;

  return (
    <div className="col-md-6 ">
      <div className="col-md-12 shadow">
        {List.todoList.map((data) => (
          <div key={data.id}>
            <h4>{data.listname}</h4>
            <div className="form-group">
              {data.todos.map((datalist) => (
                <div key={datalist.id} className="form-check">
                  <input className="form-check-input filled-in" type="checkbox" id={datalist.id} onChange={() => handleSelection(data, datalist, 'add')} checked={datalist.isCompleted} />
                  <label className="form-check-label" htmlFor={datalist.id}>
                    {datalist.name}
                  </label>
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}
